import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OwlModule } from 'ngx-owl-carousel';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { HeaderComponent } from './components/header/header.component';
import { HomeBannerComponent } from './components/home-banner/home-banner.component';
import { HomeAppointmentComponent } from './components/home-appointment/home-appointment.component';
import { HomeOffersComponent } from './components/home-offers/home-offers.component';
import { HomeConditionsComponent } from './components/home-conditions/home-conditions.component';
import { HomeChooseComponent } from './components/home-choose/home-choose.component';
import { HomePricingComponent } from './components/home-pricing/home-pricing.component';
import { HomeArticlesComponent } from './components/home-articles/home-articles.component';
import { HomeReviewsComponent } from './components/home-reviews/home-reviews.component';
import { HomeContactComponent } from './components/home-contact/home-contact.component';
import { FooterComponent } from './components/footer/footer.component'

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeBannerComponent,
        HomeAppointmentComponent,
        HomeOffersComponent,
        HomeConditionsComponent,
        HomeChooseComponent,
        HomePricingComponent,
        HomeArticlesComponent,
        HomeReviewsComponent,
        HomeContactComponent,
        FooterComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FontAwesomeModule,
        OwlModule,
        FormsModule,
        ReactiveFormsModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        BrowserAnimationsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
