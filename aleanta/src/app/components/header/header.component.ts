import { Component, OnInit } from '@angular/core';
import { Input, Output } from '@angular/core';
import * as $ from 'jquery';

@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	mailLink: string = 'support@the-aleanta.com';
	phoneNumber: any = '(925) 236 7498';
	twitterLink: string = 'https://twitter.com/tranxuansang193';
	googleLink: string = 'https://plus.google.com/';
	facebookLink: string = 'https://www.facebook.com/';
	bookLink: string = 'https://google.com/';	
	menuLink: any = [
		{ title: 'Home', name: 'Home', url: '#' },
		{ title: 'About', name: 'About', url: '#' },
		{ title: 'Services', name: 'Services', url: '#' },
		{ title: 'Departments', name: 'Departments', url: '#' },
		{ title: 'News', name: 'News', url: '#' },
		{ title: 'Contact', name: 'Contact', url: '#' }
	]

	// set optionHeader = true or false change frontend of <header></header>
	optionHeader: boolean = true;

	constructor() { }

	ngOnInit() {

	}

}
