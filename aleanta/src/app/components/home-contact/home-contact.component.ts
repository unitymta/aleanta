/// <reference types="@types/googlemaps" />
// import {} from "googlemaps";
import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
	selector: 'app-home-contact',
	templateUrl: './home-contact.component.html',
	styleUrls: ['./home-contact.component.scss']
})
export class HomeContactComponent implements OnInit {
	// @ViewChild('map-canvas') gmapElement: any;
	map: google.maps.Map;

	constructor() { }

	ngOnInit() {
		// if HTML DOM Element that contains the map is found...
		if (document.getElementById('map-canvas')) {

			// Coordinates to center the map
			var myLatlng = new google.maps.LatLng(52.525595, 13.393085);

			// Other options for the map, pretty much selfexplanatory
			var mapOptions = {
				fullscreenControl: false,
				streetViewControl: false,
				mapTypeControl: false,
				zoomControl: false,
				zoom: 14,
				center: myLatlng,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			};

			// Attach a map to the DOM Element, with the defined settings
			var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

		}
	}
}
