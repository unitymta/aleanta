import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-home-reviews',
	templateUrl: './home-reviews.component.html',
	styleUrls: ['./home-reviews.component.scss']
})
export class HomeReviewsComponent implements OnInit {
	mySlideOptions = { items: 1, dots: false, nav: true };

	constructor() { }

	ngOnInit() {
	}	

}
