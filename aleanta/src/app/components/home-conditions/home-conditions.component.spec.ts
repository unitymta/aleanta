import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeConditionsComponent } from './home-conditions.component';

describe('HomeConditionsComponent', () => {
  let component: HomeConditionsComponent;
  let fixture: ComponentFixture<HomeConditionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeConditionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
