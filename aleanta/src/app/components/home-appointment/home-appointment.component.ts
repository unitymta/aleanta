import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-home-appointment',
    templateUrl: './home-appointment.component.html',
    styleUrls: ['./home-appointment.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeAppointmentComponent implements OnInit {

    homeForms: FormGroup;
    message: any = {
        messageName: '',
        messageEmail: '',
        messageSubject: '',
        messageContent: '',
    };
    public dateTime1: Date;

    constructor(private formBuilder: FormBuilder) { }

    ngOnInit() {
        this.homeForms = this.formBuilder.group({
            fullName: ['', Validators.required],
            email: ['', Validators.required],
            content: ['', Validators.required],
            subject: ['', Validators.required]
        });
    }

    get f() { return this.homeForms.controls };

    formatForm() {
        this.message.messageName = "";
        this.message.messageEmail = "";
        this.message.messageSubject = "";
        this.message.messageContent = "";
    }

    formValidate() {
        if (this.homeForms.invalid) {
            if (this.f.email.value == '') {
                this.message.messageEmail = "Please enter your email";
            } else {
                this.message.messageEmail = "";
            }
            if (this.f.fullName.value == '') {
                this.message.messageName = "Please enter your name";
            } else {
                this.message.messageName = "";
            }
            if (this.f.subject.value == '') {
                this.message.messageSubject = "Please enter your subject";
            } else {
                this.message.messageSubject = "";
            }
            if (this.f.content.value == '') {
                this.message.messageContent = "Please enter your content";
            } else {
                this.message.messageContent = "";
            }
        }
        else {
            this.formatForm();
            alert('Success message');
        }
    }
}

